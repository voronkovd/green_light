# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='message',
            options={'ordering': ['-id'], 'verbose_name': '\u0417\u0430\u044f\u0432\u043a\u0443', 'verbose_name_plural': '\u0417\u0430\u044f\u0432\u043a\u0438'},
        ),
        migrations.AlterField(
            model_name='message',
            name='vin',
            field=models.CharField(max_length=128, null=True, verbose_name='VIN', blank=True),
            preserve_default=True,
        ),
    ]
