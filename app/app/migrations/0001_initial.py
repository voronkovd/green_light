# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Brand',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=128, verbose_name='\u041d\u0430\u0438\u043c\u0435\u043d\u043e\u0432\u0430\u043d\u0438\u0435')),
                ('status', models.BooleanField(default=False, verbose_name='\u041e\u0442\u043e\u0431\u0440\u0430\u0436\u0430\u0442\u044c \u043d\u0430 \u0441\u0430\u0439\u0442\u0435')),
                ('image', models.ImageField(upload_to=None, verbose_name='\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435')),
            ],
            options={
                'db_table': 'brands',
                'verbose_name': '\u0411\u0440\u0435\u043d\u0434',
                'verbose_name_plural': '\u0411\u0440\u0435\u043d\u0434\u044b',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Carousel',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=128, verbose_name='\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a')),
                ('anons', models.CharField(max_length=128, verbose_name='\u0410\u043d\u043e\u043d\u0441')),
                ('content', models.TextField(max_length=2000, verbose_name='\u041f\u043e\u0434\u0440\u043e\u0431\u043d\u0430\u044f \u0438\u043d\u0444\u043e\u0440\u043c\u0430\u0446\u0438\u044f')),
                ('status', models.BooleanField(default=False, verbose_name='\u041e\u0442\u043e\u0431\u0440\u0430\u0436\u0430\u0442\u044c \u043d\u0430 \u0441\u0430\u0439\u0442\u0435')),
                ('image', models.ImageField(upload_to=None, verbose_name='\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435')),
            ],
            options={
                'db_table': 'carousels',
                'verbose_name': '\u041e\u0431\u044a\u0435\u043a\u0442 \u043a\u0430\u0440\u0443\u0441\u0435\u043b\u0438',
                'verbose_name_plural': '\u041e\u0431\u044a\u0435\u043a\u0442\u044b \u043a\u0430\u0440\u0443\u0441\u0435\u043b\u0438',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Message',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('contact_name', models.CharField(max_length=128, verbose_name='\u041a\u043e\u043d\u0442\u0430\u043a\u0442\u043d\u043e\u0435 \u043b\u0438\u0446\u043e')),
                ('contact_phone', models.CharField(max_length=128, verbose_name='\u041a\u043e\u043d\u0442\u0430\u043a\u0442\u043d\u044b\u0439 \u0442\u0435\u043b\u0435\u0444\u043e\u043d')),
                ('brand_car', models.CharField(max_length=128, verbose_name='\u041c\u0430\u0440\u043a\u0430 \u0430\u0432\u0442\u043e\u043c\u043e\u0431\u0438\u043b\u044f')),
                ('model_car', models.CharField(max_length=128, verbose_name='\u041c\u043e\u0434\u0435\u043b\u044c \u0430\u0432\u0442\u043e\u043c\u043e\u0431\u0438\u043b\u044f')),
                ('year_car', models.CharField(max_length=5, verbose_name='\u0413\u043e\u0434 \u0432\u044b\u043f\u0443\u0441\u043a\u0430')),
                ('vin', models.CharField(max_length=128, verbose_name='VIN')),
                ('content', models.TextField(max_length=2000, verbose_name='\u0422\u0435\u043a\u0441\u0442 \u0437\u0430\u043f\u0440\u043e\u0441\u0430')),
                ('status', models.BooleanField(default=False, verbose_name='\u0417\u0430\u044f\u0432\u043a\u0430 \u0440\u0435\u0448\u0435\u043d\u0430')),
            ],
            options={
                'db_table': 'messages',
                'verbose_name': '\u0417\u0430\u044f\u0432\u043a\u0443',
                'verbose_name_plural': '\u0417\u0430\u044f\u0432\u043a\u0438',
            },
            bases=(models.Model,),
        ),
    ]
