# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0002_auto_20150204_1254'),
    ]

    operations = [
        migrations.AddField(
            model_name='message',
            name='comment',
            field=models.TextField(max_length=2000, null=True, verbose_name='\u0422\u0435\u043a\u0441\u0442 \u0437\u0430\u043f\u0440\u043e\u0441\u0430', blank=True),
            preserve_default=True,
        ),
    ]
