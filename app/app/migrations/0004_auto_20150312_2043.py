# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0003_message_comment'),
    ]

    operations = [
        migrations.AlterField(
            model_name='message',
            name='brand_car',
            field=models.CharField(max_length=24, verbose_name='\u041c\u0430\u0440\u043a\u0430 \u0430\u0432\u0442\u043e\u043c\u043e\u0431\u0438\u043b\u044f'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='message',
            name='comment',
            field=models.TextField(max_length=2000, null=True, verbose_name='\u041a\u043e\u043c\u043c\u0435\u043d\u0442\u0430\u0440\u0438\u0439', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='message',
            name='contact_name',
            field=models.CharField(max_length=16, verbose_name='\u041a\u043e\u043d\u0442\u0430\u043a\u0442\u043d\u043e\u0435 \u043b\u0438\u0446\u043e'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='message',
            name='contact_phone',
            field=models.CharField(max_length=16, verbose_name='\u041a\u043e\u043d\u0442\u0430\u043a\u0442\u043d\u044b\u0439 \u0442\u0435\u043b\u0435\u0444\u043e\u043d'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='message',
            name='model_car',
            field=models.CharField(max_length=24, verbose_name='\u041c\u043e\u0434\u0435\u043b\u044c \u0430\u0432\u0442\u043e\u043c\u043e\u0431\u0438\u043b\u044f'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='message',
            name='vin',
            field=models.CharField(max_length=36, null=True, verbose_name='VIN', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='message',
            name='year_car',
            field=models.CharField(max_length=4, verbose_name='\u0413\u043e\u0434 \u0432\u044b\u043f\u0443\u0441\u043a\u0430'),
            preserve_default=True,
        ),
    ]
