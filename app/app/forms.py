# -*- coding: utf-8 -*-
from django import forms
from models import Message


class MessageForm(forms.ModelForm):
    class Meta:
        model = Message
        fields = ['contact_name', 'contact_phone', 'brand_car', 'model_car', 'year_car', 'vin', 'content']