# -*- coding: utf-8 -*
from uuid import uuid4
from django.conf import settings
from django.db import models
import os


def path_and_rename(path):
    def wrapper(instance, filename):
        ext = filename.split('.')[-1]
        if instance.pk:
            filename = '{}.{}'.format(instance.pk, ext)
        else:
            filename = '{}.{}'.format(uuid4().hex, ext)
        return os.path.join(path, filename)

    return wrapper


class Carousel(models.Model):
    title = models.CharField(max_length=128, verbose_name=u'Заголовок')
    anons = models.CharField(max_length=128, verbose_name=u"Анонс")
    content = models.TextField(max_length=2000, verbose_name=u"Подробная информация")
    status = models.BooleanField(default=False, verbose_name=u"Отображать на сайте")
    image = models.ImageField(verbose_name=u"Изображение", upload_to=path_and_rename('images/carousels/'))

    class Meta:
        db_table = 'carousels'
        app_label = settings.DEFAULT_APP
        verbose_name = u'Объект карусели'
        verbose_name_plural = u'Объекты карусели'

    def __unicode__(self):
        return self.title


class Brand(models.Model):
    name = models.CharField(max_length=128, verbose_name=u"Наименование")
    status = models.BooleanField(default=False, verbose_name=u"Отображать на сайте")
    image = models.ImageField(verbose_name=u"Изображение", upload_to=path_and_rename('images/brands/'))

    class Meta:
        db_table = 'brands'
        app_label = settings.DEFAULT_APP
        verbose_name = u'Бренд'
        verbose_name_plural = u'Бренды'

    def __unicode__(self):
        return self.name


class Message(models.Model):
    contact_name = models.CharField(max_length=16, verbose_name=u"Контактное лицо")
    contact_phone = models.CharField(max_length=16, verbose_name=u"Контактный телефон")
    brand_car = models.CharField(max_length=24, verbose_name=u"Марка автомобиля")
    model_car = models.CharField(max_length=24, verbose_name=u"Модель автомобиля")
    year_car = models.CharField(max_length=4, verbose_name=u"Год выпуска")
    vin = models.CharField(max_length=36, verbose_name=u"VIN", blank=True, null=True)
    content = models.TextField(max_length=2000, verbose_name=u"Текст запроса")
    status = models.BooleanField(default=False, verbose_name=u"Заявка решена")
    comment = models.TextField(max_length=2000, verbose_name=u"Комментарий", null=True, blank=True)

    class Meta:
        db_table = 'messages'
        app_label = settings.DEFAULT_APP
        verbose_name = u'Заявку'
        verbose_name_plural = u'Заявки'
        ordering = ['-id']

    def __unicode__(self):
        return self.contact_name