# -*- coding: utf-8 -*-
import json

from django.template.response import TemplateResponse, HttpResponse
from django.http import Http404
from django.views.decorators.csrf import csrf_exempt
from models import Brand, Carousel
from forms import MessageForm
from django.core.mail import send_mail
from django.core.mail import EmailMultiAlternatives
from django.template import Context
from django.template.loader import render_to_string


def json_response(x):
    return HttpResponse(json.dumps(x, sort_keys=True, indent=2), content_type='application/json; charset=UTF-8')


def index(request):
    brands = Brand.objects.filter(status=1).order_by('-id')
    carousels = Carousel.objects.filter(status=1).order_by('-id')
    return TemplateResponse(request, 'index.html', {'brands': brands, 'carousels': carousels})


@csrf_exempt
def contact(request):
    if request.method == 'POST':
        form = MessageForm(data=request.POST)
        if form.is_valid():
            model = form.save()
            try:
                c = Context({'model': model})
                text_content = render_to_string('mail/email.txt', c)
                html_content = render_to_string('mail/email.html', c)

                email = EmailMultiAlternatives(u'Автомагазин "Зеленый свет"', text_content, 'robot@glshop.ru')
                email.attach_alternative(html_content, "text/html")
                email.to = ['orders@glshop.ru']
                email.send()
            except Exception:
                pass
            data = {'status': True, 'message': u'Ваша заявка принята. Мы свяжемся с вами в ближайшее время.'}
        else:
            data = {'status': False, 'message': '', 'errors': form.errors}
        return json_response(data)
    else:
        raise Http404


def discont(request):
    return TemplateResponse(request, 'discont.html')