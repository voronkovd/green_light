# -*- coding: utf-8 -*-
from models import Carousel, Message, Brand
from django.contrib import admin
from django.forms import ModelForm
from django.contrib.admin import ModelAdmin
from suit_redactor.widgets import RedactorWidget
from django.utils.safestring import mark_safe
from django.contrib.admin.models import LogEntry


try:
    import json
except ImportError:
    import django.utils.simplejson as json


class MyRedactorWidget(RedactorWidget):
    class Media:
        def __init__(self):
            pass

        css = {
            'all': ('suit-redactor/redactor/redactor.css',)
        }
        js = ('suit-redactor/redactor/redactor.js',)

    def __init__(self, attrs=None, editor_options={}):
        super(RedactorWidget, self).__init__(attrs)
        self.editor_options = editor_options

    def render(self, name, value, attrs=None):
        output = super(RedactorWidget, self).render(name, value, attrs)
        output += mark_safe(
            '<script type="text/javascript">$("#id_%s").redactor(%s);</script>'
            % (name, json.dumps(self.editor_options)))
        return output


class CarouselForm(ModelForm):
    class Meta:
        widgets = {'content': MyRedactorWidget(editor_options={'lang': 'ru', 'minHeight': 300})}


class MessageForm(ModelForm):
    class Meta:
        model = Message
        fields = ['contact_phone', 'contact_name', 'brand_car', 'model_car', 'year_car', 'vin', 'content', 'status', 'comment']
        widgets = {
            'comment': MyRedactorWidget(editor_options={'lang': 'ru', 'minHeight': 300}),
        }


class CarouseAdmin(ModelAdmin):
    list_display = ('title', 'status')
    list_filter = ('title', 'status')
    form = CarouselForm
    list_select_related = True
    list_per_page = 10
    list_max_show_all = 30
    save_as = False


class MessageAdmin(ModelAdmin):
    form = MessageForm
    list_display = ('contact_name', 'contact_phone', 'brand_car', 'model_car', 'year_car', 'vin', 'status')
    list_filter = ('contact_name', 'contact_phone', 'brand_car', 'model_car', 'year_car', 'vin', 'status')
    list_select_related = True
    list_per_page = 10
    list_max_show_all = 30
    save_as = False
    show_save_as_new = False
    show_delete_link = False
    show_save_and_add_another = False
    show_save_and_continue = False


class LogEntryAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'user', 'content_type', 'object_id', 'object_repr', 'change_message')
    date_hierarchy = 'action_time'


admin.site.register(LogEntry, LogEntryAdmin)

admin.site.register(Brand)
admin.site.register(Carousel, CarouseAdmin)
admin.site.register(Message, MessageAdmin)